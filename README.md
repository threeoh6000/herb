# herb
The web server yer mom uses.

## In all seriousness!
This is a little fun experiment for me to mess around with Rust 

## Chonklist
I need this stuff to actually have a functioning but basic web server.

[x] Open a socket

[x] Recieve HTTP requests

[x] Process said requests

[x] Send back HTTP requests

[x] Read index page from filesystem

### Next Chonklist
The stuff I need to make it usable.

[x] Read the stream

[x] Detect which resource the client wants to access

[x] Detect missing files and return a 404 page

[ ] Custom error pages

[ ] Make it user configurable

[ ] Properly generate headers

[x] Read and serve other pages from filesystem

### SUPAR Chonklist
Whatever is on here, just to make it extra spicy.

[ ] HTTPS support

[ ] HTTP/2 support

[ ] Dynamic pages via CGI?

[ ] Image thumbnailing/compression

[ ] Compressing big files

[x] Directory index generation
